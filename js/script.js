/*Script de gestion des fondus lorsque l'utilisateur arrive sur un élément*/
$(window).scroll(function () {
    $('.fadein').each(function (i) {

        var haut_element = $(this).offset().top;
        var bas_ecran = $(window).scrollTop() + $(window).height();

        if (bas_ecran > haut_element) {
            $(this).animate({ 'opacity': '1' }, 1000);
        }

    })
});


/*Script de gestion du formulaire pop-up*/
function openForm() {
    document.getElementById("formulaireContact").style.display = "block";
}

function closeForm() {
    document.getElementById("formulaireContact").style.display = "none";
}

/*Script de gestion du bandeau cookies (selon tuto de la w3school) 
On stocke le bandeau*/
var modal = document.getElementById("myModal");
/*On stocke l'élément qui ferme le bandeau*/
var span = document.getElementsByClassName("close")[0];

/*Ferme le bandeau lorsque l'utilisateur clique sur X*/
span.onclick = function () {
    modal.style.display = "none";
}
/*Ferme le bandau lorsque l'utilisateur clique n'importe ou sur l'écran*/
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

/*Gestion des boites de confirmation lorsque l'on sort du site*/
function openLinkedin() {
    var txt;
    if (confirm("Vous allez quitter le site")) {
        window.location.href ="http://www.linkedin.com/in/ivan-béthus-570067b2";
    }
  }
