<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="Description" content="Discover the creative online resume of Ivan Bethus, software developer.">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" media="screen" href="css/main.min.css" />
  <link rel="stylesheet" media="print" href="css/print.css" />
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="fonts/stylesheet.css" type="text/css" />
  <title>Ivan Bethus</title>

</head>

<body>
  <!--***************** Header *****************-->
  <header>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="#">
        <img src="img/logoIB.webp" width="30" height="30" alt="Logo du site">
      </a>

      <!-- Bouton de gestion du collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav, #groupButton" aria-controls="navbarNav" aria-expanded="false" aria-label="Menu">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Fin du bouton -->

      <!-- Liste des liens -->
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
            <a class="nav-link" href="#pres">Presentation</a>
          </li>
          <li class="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
            <a class="nav-link" href="#competences">Skills</a>
          </li>
          <li class="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
            <a class="nav-link" href="#projets">Projects</a>
          </li>
          <li class="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
            <a class="nav-link" href="#diplomes">Experience</a>
          </li>
          <li class="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
            <a class="nav-link" href="#contact">Contact</a>
          </li>
          <li class="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
            <a class="nav-link" href="download/cv_info.pdf" download>CV (pdf 250Ko)</a>
          </li>
        </ul>
      </div>
      <!-- Fin de la liste -->

      <!-- Switch entre les langues -->
      <div class="btn-group btn-group-toggle collapse navbar-collapse" data-toggle="buttons" id="groupButton">
        <label class="btn btn-secondary active">
          <input type="radio" name="français" id="option1"> <a href="index.php"> Français
          </a>
        </label>
        <label class="btn btn-secondary">
          <input type="radio" name="english" id="option2" checked> English
        </label>
      </div>
      <!-- Fin du switch -->
    </nav>
    <!-- Bandeau cookies -->
    <div id="myModal" class="modal">

      <!-- Contenu du bandeau -->
      <div class="modal-content">
        <span class="close">x</span>
        <div>
          <p> <img src="img/icones/cookie.svg" alt="Logo coookie">
            Cookies are sweet, but you won't find any on this website !</p>
        </div>
      </div>

    </div>
  </header>
  <!--***************** Fin header *****************-->

  <!--********************* Main ********************-->

  <div class="parallax-main">
    <div class="centered">
      <div class="col-md">
        <div class="row">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="250 0 700 150">
            <path id="Chemin" fill="white" stroke="white" stroke-width="2" d="M 185.08,147.00
                      C 185.08,147.00 185.08,42.00 185.08,42.00
                        185.08,42.00 159.48,42.00 159.48,42.00
                        159.48,42.00 159.48,147.00 159.48,147.00
                        159.48,147.00 185.08,147.00 185.08,147.00 Z
                      M 273.19,42.00
                      C 273.19,42.00 247.47,96.08 247.47,96.08
                        247.47,96.08 221.81,42.00 221.81,42.00
                        221.81,42.00 194.56,42.00 194.56,42.00
                        194.56,42.00 244.80,147.00 244.80,147.00
                        244.80,147.00 250.27,147.00 250.27,147.00
                        250.27,147.00 300.70,42.00 300.70,42.00
                        300.70,42.00 273.19,42.00 273.19,42.00 Z
                      M 327.16,96.00
                      C 327.16,96.00 333.86,110.16 333.86,110.16
                        333.86,110.16 320.38,110.16 320.38,110.16
                        320.38,110.16 327.16,96.00 327.16,96.00 Z
                      M 301.30,147.00
                      C 301.30,147.00 309.06,131.00 309.06,131.00
                        309.06,131.00 345.14,131.00 345.14,131.00
                        345.14,131.00 352.91,147.00 352.91,147.00
                        352.91,147.00 380.67,147.00 380.67,147.00
                        380.67,147.00 330.12,42.36 330.12,42.36
                        330.12,42.36 324.08,42.36 324.08,42.36
                        324.08,42.36 273.59,147.00 273.59,147.00
                        273.59,147.00 301.30,147.00 301.30,147.00 Z
                      M 389.48,41.00
                      C 389.48,41.00 389.48,147.00 389.48,147.00
                        389.48,147.00 415.84,147.00 415.84,147.00
                        415.84,147.00 415.84,91.48 415.84,91.48
                        415.84,91.48 476.98,147.00 476.98,147.00
                        476.98,147.00 482.75,147.00 482.75,147.00
                        482.75,147.00 482.75,41.00 482.75,41.00
                        482.75,41.00 456.31,41.00 456.31,41.00
                        456.31,41.00 456.31,96.66 456.31,96.66
                        456.31,96.66 395.03,41.00 395.03,41.00
                        395.03,41.00 389.48,41.00 389.48,41.00 Z
                      M 598.19,84.84
                      C 600.02,81.67 601.89,76.23 601.89,71.81
                        601.89,63.86 598.80,56.86 591.94,51.05
                        584.80,45.06 576.03,42.02 565.98,42.02
                        565.98,42.02 534.48,42.00 534.48,42.00
                        534.48,42.00 534.48,147.00 534.48,147.00
                        534.48,147.00 577.19,147.00 577.19,147.00
                        588.34,147.00 598.03,143.62 604.94,137.03
                        611.69,130.52 615.14,122.45 615.14,113.17
                        615.14,101.25 608.45,89.66 598.19,84.84 Z
                      M 577.19,121.94
                      C 577.19,121.94 560.02,121.94 560.02,121.94
                        560.02,121.94 560.02,101.02 560.02,101.02
                        565.91,100.97 570.95,100.80 577.03,101.08
                        580.12,101.27 582.75,102.27 585.25,104.38
                        587.52,106.42 588.62,108.78 588.62,111.70
                        588.62,117.69 583.31,121.94 577.19,121.94 Z
                      M 573.58,73.95
                      C 573.58,78.27 569.58,80.89 565.38,81.34
                        563.34,81.50 561.58,81.62 560.02,81.73
                        560.02,81.73 560.02,67.16 560.02,67.16
                        560.02,67.16 565.98,67.16 565.98,67.16
                        569.98,67.16 573.58,70.09 573.58,73.95 Z
                      M 624.48,148.00
                      C 624.48,148.00 696.70,148.00 696.70,148.00
                        696.70,148.00 696.70,122.55 696.70,122.55
                        696.70,122.55 651.30,122.55 651.30,122.55
                        651.30,122.55 651.30,107.53 651.30,107.53
                        651.30,107.53 686.19,107.53 686.19,107.53
                        686.19,107.53 686.19,82.00 686.19,82.00
                        686.19,82.00 651.30,82.00 651.30,82.00
                        651.30,82.00 651.30,66.53 651.30,66.53
                        651.30,66.53 696.70,66.53 696.70,66.53
                        696.70,66.53 696.70,41.00 696.70,41.00
                        696.70,41.00 624.48,41.00 624.48,41.00
                        624.48,41.00 624.48,148.00 624.48,148.00 Z
                      M 701.59,67.41
                      C 701.59,67.41 728.36,67.41 728.36,67.41
                        728.36,67.41 728.67,147.00 728.67,147.00
                        728.67,147.00 757.56,147.00 757.56,147.00
                        757.56,147.00 756.59,67.41 756.59,67.41
                        756.59,67.41 783.28,67.41 783.28,67.41
                        783.28,67.41 783.28,42.00 783.28,42.00
                        783.28,42.00 701.59,42.00 701.59,42.00
                        701.59,42.00 701.59,67.41 701.59,67.41 Z
                      M 792.48,147.00
                      C 792.48,147.00 818.02,147.00 818.02,147.00
                        818.02,147.00 818.02,107.00 818.02,107.00
                        818.02,107.00 853.02,107.00 853.02,107.00
                        853.02,107.00 853.02,147.00 853.02,147.00
                        853.02,147.00 878.53,147.00 878.53,147.00
                        878.53,147.00 878.53,42.00 878.53,42.00
                        878.53,42.00 853.02,42.00 853.02,42.00
                        853.02,42.00 853.02,81.61 853.02,81.61
                        853.02,81.61 818.02,81.61 818.02,81.61
                        818.02,81.61 818.02,42.00 818.02,42.00
                        818.02,42.00 792.48,42.00 792.48,42.00
                        792.48,42.00 792.48,147.00 792.48,147.00 Z
                      M 938.38,122.16
                      C 932.52,122.16 927.64,120.45 923.44,116.94
                        919.23,113.33 917.20,109.20 917.20,104.33
                        917.20,104.33 917.20,42.00 917.20,42.00
                        917.20,42.00 891.48,42.00 891.48,42.00
                        891.48,42.00 891.48,105.22 891.48,105.22
                        891.48,117.02 895.89,127.23 904.56,135.55
                        913.23,143.81 924.67,148.00 938.45,148.00
                        952.08,148.00 963.47,143.81 972.19,135.55
                        980.86,127.22 985.27,117.02 985.27,105.22
                        985.27,105.22 985.27,42.00 985.27,42.00
                        985.27,42.00 959.55,42.00 959.55,42.00
                        959.55,42.00 959.55,104.33 959.55,104.33
                        959.55,114.59 948.98,122.16 938.38,122.16 Z
                      M 1020.67,111.03
                      C 1020.67,111.03 993.92,116.55 993.92,116.55
                        993.92,116.55 994.23,118.62 994.23,118.62
                        996.80,135.42 1012.70,148.00 1031.84,148.00
                        1052.70,148.00 1069.73,133.67 1069.73,115.61
                        1069.73,100.39 1057.05,90.12 1042.11,86.33
                        1033.27,84.06 1022.58,81.81 1022.58,74.83
                        1022.58,70.38 1026.86,66.72 1031.92,66.72
                        1038.53,66.72 1041.16,71.44 1041.16,77.94
                        1041.16,77.94 1068.05,72.47 1068.05,72.47
                        1068.05,72.47 1067.77,70.41 1067.77,70.41
                        1065.47,53.78 1050.92,41.02 1031.78,41.02
                        1010.78,41.02 995.39,55.44 995.39,73.38
                        995.39,88.25 1007.61,98.88 1022.14,103.23
                        1031.42,106.00 1042.75,107.08 1042.75,114.09
                        1042.75,118.17 1037.03,122.19 1031.67,122.19
                        1031.67,122.19 1030.30,122.19 1030.30,122.19
                        1025.30,121.53 1020.67,117.70 1020.67,113.80
                        1020.67,113.80 1020.67,111.03 1020.67,111.03 Z" />
          </svg>
        </div>
        <div class="row">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="200 150 800 100">
            <path id="Software_developer" fill="white" stroke="white" stroke-width="2" d="M 246.36,221.95
                              C 246.36,221.95 233.89,224.44 233.89,224.44
                                233.89,224.44 234.05,225.38 234.05,225.38
                                235.23,233.14 242.64,239.00 251.58,239.00
                                261.30,239.00 269.23,232.33 269.23,224.02
                                269.23,217.16 263.31,212.55 256.36,210.83
                                252.23,209.81 247.25,208.80 247.25,205.66
                                247.25,203.64 249.25,202.00 251.61,202.00
                                254.69,202.00 255.91,204.12 255.91,207.05
                                255.91,207.05 268.45,204.59 268.45,204.59
                                268.45,204.59 268.31,203.67 268.31,203.67
                                267.23,195.97 260.47,190.02 251.55,190.02
                                241.75,190.02 234.58,196.73 234.58,205.00
                                234.58,211.69 240.27,216.48 247.05,218.44
                                251.38,219.69 256.66,220.17 256.66,223.33
                                256.66,225.16 253.98,226.97 251.48,226.97
                                251.48,226.97 250.84,226.97 250.84,226.97
                                248.52,226.67 246.36,224.95 246.36,223.19
                                246.36,223.19 246.36,221.95 246.36,221.95 Z
                              M 278.78,197.30
                              C 273.75,202.16 271.20,207.92 271.20,214.56
                                271.20,221.22 273.75,226.98 278.78,231.80
                                284.09,236.56 289.66,238.98 297.17,239.00
                                304.67,239.00 310.25,236.59 315.56,231.80
                                320.62,227.00 323.19,221.23 323.19,214.56
                                323.19,207.91 320.62,202.14 315.56,197.30
                                310.23,192.47 304.66,190.00 297.17,190.00
                                289.70,190.00 284.12,192.45 278.78,197.30 Z
                              M 306.55,205.69
                              C 309.45,208.17 310.88,211.06 310.88,214.50
                                310.88,217.98 309.45,220.88 306.56,223.33
                                303.66,225.77 301.19,226.95 297.17,226.95
                                293.09,226.95 290.62,225.77 287.77,223.33
                                284.89,220.86 283.48,217.97 283.48,214.50
                                283.48,211.08 284.89,208.19 287.77,205.69
                                290.61,203.23 293.08,202.03 297.17,202.03
                                301.20,202.03 303.67,203.22 306.55,205.69 Z
                              M 327.02,239.00
                              C 327.02,239.00 339.52,239.00 339.52,239.00
                                339.52,239.00 339.52,220.91 339.52,220.91
                                339.52,220.91 355.78,220.91 355.78,220.91
                                355.78,220.91 355.78,209.00 355.78,209.00
                                355.78,209.00 339.52,209.00 339.52,209.00
                                339.52,209.00 339.52,201.91 339.52,201.91
                                339.52,201.91 360.69,201.91 360.69,201.91
                                360.69,201.91 360.69,190.00 360.69,190.00
                                360.69,190.00 327.02,190.00 327.02,190.00
                                327.02,190.00 327.02,239.00 327.02,239.00 Z
                              M 363.20,201.84
                              C 363.20,201.84 375.69,201.84 375.69,201.84
                                375.69,201.84 375.83,239.00 375.83,239.00
                                375.83,239.00 389.30,239.00 389.30,239.00
                                389.30,239.00 388.84,201.84 388.84,201.84
                                388.84,201.84 401.28,201.84 401.28,201.84
                                401.28,201.84 401.28,190.00 401.28,190.00
                                401.28,190.00 363.20,190.00 363.20,190.00
                                363.20,190.00 363.20,201.84 363.20,201.84 Z
                              M 424.64,239.00
                              C 424.64,239.00 427.08,239.00 427.08,239.00
                                427.08,239.00 435.34,217.70 435.34,217.70
                                435.34,217.70 443.83,239.00 443.83,239.00
                                443.83,239.00 446.27,239.00 446.27,239.00
                                446.27,239.00 466.69,190.00 466.69,190.00
                                466.69,190.00 453.81,190.00 453.81,190.00
                                453.81,190.00 445.03,211.34 445.03,211.34
                                445.03,211.34 436.03,189.52 436.03,189.52
                                436.03,189.52 434.91,189.52 434.91,189.52
                                434.91,189.52 425.86,211.34 425.86,211.34
                                425.86,211.34 417.06,190.00 417.06,190.00
                                417.06,190.00 404.22,190.00 404.22,190.00
                                404.22,190.00 424.64,239.00 424.64,239.00 Z
                              M 481.17,215.41
                              C 481.17,215.41 484.30,222.00 484.30,222.00
                                484.30,222.00 478.02,222.00 478.02,222.00
                                478.02,222.00 481.17,215.41 481.17,215.41 Z
                              M 469.12,239.00
                              C 469.12,239.00 472.73,231.72 472.73,231.72
                                472.73,231.72 489.56,231.72 489.56,231.72
                                489.56,231.72 493.17,239.00 493.17,239.00
                                493.17,239.00 506.12,239.00 506.12,239.00
                                506.12,239.00 482.56,190.41 482.56,190.41
                                482.56,190.41 479.73,190.41 479.73,190.41
                                479.73,190.41 456.20,239.00 456.20,239.00
                                456.20,239.00 469.12,239.00 469.12,239.00 Z
                              M 521.97,239.00
                              C 521.97,239.00 521.97,219.83 521.97,219.83
                                521.97,219.83 536.47,239.00 536.47,239.00
                                536.47,239.00 550.98,239.00 550.98,239.00
                                550.98,239.00 539.25,221.41 539.25,221.41
                                541.55,220.36 542.84,219.30 544.39,217.19
                                546.56,214.23 547.67,211.03 547.67,207.64
                                547.67,207.64 547.67,206.42 547.67,206.42
                                547.67,200.92 545.72,198.02 542.64,195.06
                                540.84,193.36 538.75,192.08 536.36,191.25
                                533.61,190.42 530.77,190.00 527.86,190.00
                                527.86,190.00 510.02,190.00 510.02,190.00
                                510.02,190.00 510.05,239.00 510.05,239.00
                                510.05,239.00 521.97,239.00 521.97,239.00 Z
                              M 528.95,213.28
                              C 528.95,213.28 526.38,213.28 526.38,213.28
                                526.38,213.28 526.38,213.25 526.38,213.25
                                526.38,213.25 521.94,213.25 521.94,213.25
                                521.94,213.25 521.94,201.84 521.94,201.84
                                521.94,201.84 527.16,201.84 527.16,201.84
                                529.38,201.84 532.09,202.42 533.36,203.47
                                534.64,204.64 535.27,205.92 535.27,207.44
                                535.27,210.81 532.22,213.28 528.95,213.28 Z
                              M 554.02,239.00
                              C 554.02,239.00 587.69,239.00 587.69,239.00
                                587.69,239.00 587.69,227.14 587.69,227.14
                                587.69,227.14 566.52,227.14 566.52,227.14
                                566.52,227.14 566.52,220.91 566.52,220.91
                                566.52,220.91 582.78,220.91 582.78,220.91
                                582.78,220.91 582.78,209.00 582.78,209.00
                                582.78,209.00 566.52,209.00 566.52,209.00
                                566.52,209.00 566.52,201.91 566.52,201.91
                                566.52,201.91 587.69,201.91 587.69,201.91
                                587.69,201.91 587.69,190.00 587.69,190.00
                                587.69,190.00 554.02,190.00 554.02,190.00
                                554.02,190.00 554.02,239.00 554.02,239.00 Z
                              M 610.02,190.20
                              C 610.02,190.20 610.02,239.00 610.02,239.00
                                610.02,239.00 624.06,239.00 624.06,239.00
                                633.12,239.00 640.84,236.62 646.88,231.86
                                651.47,227.50 653.73,221.69 653.73,214.62
                                653.73,207.58 651.47,201.75 646.89,197.34
                                642.00,193.09 634.80,190.17 626.19,190.17
                                626.19,190.17 625.12,190.17 625.12,190.17
                                625.12,190.17 625.12,190.20 625.12,190.20
                                625.12,190.20 610.02,190.20 610.02,190.20 Z
                              M 641.31,214.56
                              C 640.20,222.83 634.11,227.11 624.44,227.11
                                624.44,227.11 623.12,227.11 623.12,227.11
                                623.12,227.11 623.02,202.00 623.02,202.00
                                623.02,202.00 626.16,202.00 626.16,202.00
                                634.69,202.00 641.31,206.42 641.31,214.56 Z
                              M 658.02,239.00
                              C 658.02,239.00 691.69,239.00 691.69,239.00
                                691.69,239.00 691.69,227.14 691.69,227.14
                                691.69,227.14 670.52,227.14 670.52,227.14
                                670.52,227.14 670.52,220.91 670.52,220.91
                                670.52,220.91 686.78,220.91 686.78,220.91
                                686.78,220.91 686.78,209.00 686.78,209.00
                                686.78,209.00 670.52,209.00 670.52,209.00
                                670.52,209.00 670.52,201.91 670.52,201.91
                                670.52,201.91 691.69,201.91 691.69,201.91
                                691.69,201.91 691.69,190.00 691.69,190.00
                                691.69,190.00 658.02,190.00 658.02,190.00
                                658.02,190.00 658.02,239.00 658.02,239.00 Z
                              M 730.84,190.00
                              C 730.84,190.00 718.86,215.25 718.86,215.25
                                718.86,215.25 706.89,190.00 706.89,190.00
                                706.89,190.00 694.19,190.00 694.19,190.00
                                694.19,190.00 717.61,239.00 717.61,239.00
                                717.61,239.00 720.16,239.00 720.16,239.00
                                720.16,239.00 743.67,190.00 743.67,190.00
                                743.67,190.00 730.84,190.00 730.84,190.00 Z
                              M 748.02,239.00
                              C 748.02,239.00 781.69,239.00 781.69,239.00
                                781.69,239.00 781.69,227.14 781.69,227.14
                                781.69,227.14 760.52,227.14 760.52,227.14
                                760.52,227.14 760.52,220.91 760.52,220.91
                                760.52,220.91 776.78,220.91 776.78,220.91
                                776.78,220.91 776.78,209.00 776.78,209.00
                                776.78,209.00 760.52,209.00 760.52,209.00
                                760.52,209.00 760.52,201.91 760.52,201.91
                                760.52,201.91 781.69,201.91 781.69,201.91
                                781.69,201.91 781.69,190.00 781.69,190.00
                                781.69,190.00 748.02,190.00 748.02,190.00
                                748.02,190.00 748.02,239.00 748.02,239.00 Z
                              M 786.02,239.00
                              C 786.02,239.00 819.69,239.00 819.69,239.00
                                819.69,239.00 819.69,227.14 819.69,227.14
                                819.69,227.14 797.95,227.14 797.95,227.14
                                797.95,227.14 797.95,190.00 797.95,190.00
                                797.95,190.00 786.02,190.00 786.02,190.00
                                786.02,190.00 786.02,239.00 786.02,239.00 Z
                              M 826.78,197.30
                              C 821.75,202.16 819.20,207.92 819.20,214.56
                                819.20,221.22 821.75,226.98 826.78,231.80
                                832.09,236.56 837.66,238.98 845.17,239.00
                                852.67,239.00 858.25,236.59 863.56,231.80
                                868.62,227.00 871.19,221.23 871.19,214.56
                                871.19,207.91 868.62,202.14 863.56,197.30
                                858.23,192.47 852.66,190.00 845.17,190.00
                                837.70,190.00 832.12,192.45 826.78,197.30 Z
                              M 854.55,205.69
                              C 857.45,208.17 858.88,211.06 858.88,214.50
                                858.88,217.98 857.45,220.88 854.56,223.33
                                851.66,225.77 849.19,226.95 845.17,226.95
                                841.09,226.95 838.62,225.77 835.77,223.33
                                832.89,220.86 831.48,217.97 831.48,214.50
                                831.48,211.08 832.89,208.19 835.77,205.69
                                838.61,203.23 841.08,202.03 845.17,202.03
                                849.20,202.03 851.67,203.22 854.55,205.69 Z
                              M 886.95,239.00
                              C 886.95,239.00 886.95,225.00 886.95,225.00
                                886.95,225.00 894.00,225.00 894.00,225.00
                                899.58,225.00 904.45,223.27 908.19,219.86
                                911.75,216.52 913.56,212.33 913.56,207.53
                                913.56,202.78 911.75,198.59 908.19,195.16
                                904.53,191.75 899.77,190.00 894.27,190.00
                                894.27,190.00 875.02,190.00 875.02,190.00
                                875.02,190.00 875.02,239.00 875.02,239.00
                                875.02,239.00 886.95,239.00 886.95,239.00 Z
                              M 901.19,207.53
                              C 901.19,210.83 897.78,213.20 894.28,213.20
                                894.28,213.20 887.39,213.17 887.39,213.17
                                887.39,213.17 887.39,201.84 887.39,201.84
                                887.39,201.84 894.27,201.84 894.27,201.84
                                897.77,201.84 901.19,204.25 901.19,207.53 Z
                              M 918.02,239.00
                              C 918.02,239.00 951.69,239.00 951.69,239.00
                                951.69,239.00 951.69,227.14 951.69,227.14
                                951.69,227.14 930.52,227.14 930.52,227.14
                                930.52,227.14 930.52,220.91 930.52,220.91
                                930.52,220.91 946.78,220.91 946.78,220.91
                                946.78,220.91 946.78,209.00 946.78,209.00
                                946.78,209.00 930.52,209.00 930.52,209.00
                                930.52,209.00 930.52,201.91 930.52,201.91
                                930.52,201.91 951.69,201.91 951.69,201.91
                                951.69,201.91 951.69,190.00 951.69,190.00
                                951.69,190.00 918.02,190.00 918.02,190.00
                                918.02,190.00 918.02,239.00 918.02,239.00 Z
                              M 967.97,239.00
                              C 967.97,239.00 967.97,219.83 967.97,219.83
                                967.97,219.83 982.47,239.00 982.47,239.00
                                982.47,239.00 996.98,239.00 996.98,239.00
                                996.98,239.00 985.25,221.41 985.25,221.41
                                987.55,220.36 988.84,219.30 990.39,217.19
                                992.56,214.23 993.67,211.03 993.67,207.64
                                993.67,207.64 993.67,206.42 993.67,206.42
                                993.67,200.92 991.72,198.02 988.64,195.06
                                986.84,193.36 984.75,192.08 982.36,191.25
                                979.61,190.42 976.77,190.00 973.86,190.00
                                973.86,190.00 956.02,190.00 956.02,190.00
                                956.02,190.00 956.05,239.00 956.05,239.00
                                956.05,239.00 967.97,239.00 967.97,239.00 Z
                              M 974.95,213.28
                              C 974.95,213.28 972.38,213.28 972.38,213.28
                                972.38,213.28 972.38,213.25 972.38,213.25
                                972.38,213.25 967.94,213.25 967.94,213.25
                                967.94,213.25 967.94,201.84 967.94,201.84
                                967.94,201.84 973.16,201.84 973.16,201.84
                                975.38,201.84 978.09,202.42 979.36,203.47
                                980.64,204.64 981.27,205.92 981.27,207.44
                                981.27,210.81 978.22,213.28 974.95,213.28 Z" />
          </svg>
        </div>
        <div class="row">
          <img id="photoIvan" src="img/ivan_portrait.webp" alt="Ivan Béthus">
        </div>
      </div>
    </div>
    <div class="centered-bottom">
      <a href="#pres">
        <img src="img/icones/arrow.svg" alt="Flèche pour descendre vers le bas du site">
      </a>
    </div>
  </div>

  <!--***************** Présentation ********************-->
  <div id="pres" class="row">
    <div class="col-md">
      <div class="col-md">
        <h2>Who am I ?</h2>
      </div>
      <div class="col-md">
        <div class="shadow p-4 rounded fadein">
          <p> I am a young software developeur, who freshly achived an IT technical degree from the university of Bordeaux. I am specialized in object oriented programing, mainly in Java, but I am open to any other languages !
            I am ready to work in your company, either for short or long-term missions. Simply contact me if your have a job offer you think would fit !</p>
        </div>
      </div>
    </div>
  </div>
  <!--***************** Présentation ********************-->
  <div class="parallax-last"></div>
  <!--***************** Compétences ********************-->
  <div id="competences" class="row">
    <div class="col-md">
      <h2>Skills</h2>
    </div>
    <div class="container-fluid">

      <div class="row ">
        <div class="col-md-3 fadein">
          <img src="img/icones/gear.svg" alt="Icone langages de programmation">
          <h3>Programming languages</h3>
          <p>Java, C#, C</p>
        </div>
        <div class="col-md-3 fadein">
          <img src="img/icones/internet.svg" alt="Icone technologies web">
          <h3>Web Technologies</h3>
          <p>HTML, CSS, JavaScript</p>
        </div>
        <div class="col-md-3 fadein">
          <img src="img/icones/smartphone.svg" alt="Icone programmation mobile">
          <h3>Smartphone programming</h3>
          <p>Android, Cordova</p>
        </div>
        <div class="col-md-3 fadein">
          <img src="img/icones/server.svg" alt="Icone serveur">
          <h3>Database management</h3>
          <p>Oracle, SQL Server, MySQL</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3 fadein">
          <img src="img/icones/IDE.svg" alt="Icone IDE">
          <h3>Integrated Developement Environment</h3>
          <p>Eclipse, Visual Studio Code, NetBeans, InteliJ</p>
        </div>
        <div class="col-md-3 fadein">
          <img src="img/icones/framework.svg" alt="Icone framework">
          <h3>Frameworks</h3>
          <p>ASP.NET, Java EE</p>
        </div>
        <div class="col-md-3 fadein">
          <img src="img/icones/share.svg" alt="Icone réseaux">
          <h3>Networks architecture</h3>
          <p>TCP/IP, OSI model, defining & using protocols</p>
        </div>
        <div class="col-md-3 fadein">
          <img src="img/icones/modeling.svg" alt="Icone modélisation">
          <h3>Modeling methods</h3>
          <p>UML, Merise, MS Project, Agile methods</p>
        </div>
      </div>
    </div>
  </div>
  <!--***************** Compétences ********************-->
  <div class="parallax-last"></div>
  <!--***************** Projets ********************-->
  <div id="projets" class="row">
    <div class="col-md">
      <h2>Projects</h2>
    </div>
    <div class="row">
      <div class="col-md">
        <img src="img/img_png/hexaconf.webp" alt="Site Hexaconf" class="image">
        <div class="overlay">
          <div class="text">
            <h3>Hexaconf</h3>
            <p> Integration of the fictionnal Hexaconf page.</p>
            <p>HTML, CSS</p>
          </div>
        </div>
      </div>
      <div class="col-md">
        <img src="img/img_png/page404.webp" alt="Page 404" class="image">
        <div class="overlay">
          <div class="text">
            <h3>404 Page</h3>
            <p> One of the winning project of the 2019 French IT night. Creation of a 404 error page (team work).</p>
            <p>HTML, CSS</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <img src="img/img_png/projet_SI.webp" alt="Projet Space Invader" class="image">
        <div class="overlay">
          <div class="text">
            <h3>Space Invaders</h3>
            <p> Remake of the classic space invaders game.</p>
            <p>Java</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <p>
          <img src="img/img_png/site_IUT.webp" alt="Site de l'IUT" class="image">
        </p>
        <div class="overlay">
          <div class="text">
            <h3>University website</h3>
            <p>Integration of the Bordeaux university website.</p>
            <p>HTML, CSS</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <img src="img/img_png/tron.webp" alt="Projet Tron" class="image">
        <div class="overlay">
          <div class="text">
            <h3>Tron</h3>
            <p> Remake of the Tron game. </p>
            <p>Java</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--***************** Projets ********************-->
  <div class="parallax-last"></div>
  <!--***************** Expériences ********************-->
  <div id="diplomes" class="row">
    <div class="col-md">
      <h2>Experience</h2>
      <div>
        <div>
          <img id="gear" src="img/icones/technical-support.svg" alt="icone engrenage">
        </div>
        <span onclick="javascript:if (confirm('You are leaving this webpage')) { window.location.href='https://www.iut.u-bordeaux.fr/info/';};">IT technical degree (work & study program) </span>
        <p> IUT Bordeaux (2019-2021)</p>
      </div>
      <div>
        <div>
          <img id="CCM" src="img/icones/seo-and-web.svg" alt="icone CCM">
        </div>
        <span onclick="javascript:if (confirm('You are leaving this webpage')) { window.location.href='https://www.groupeonepoint.com/en';};">CCM (Customer Communications Management) developer at OnePoint, using ExStream</span>
        <p> Bordeaux (2019-2021)</p>
      </div>
      <div>
        <div>
          <img id="books" src="img/icones/book.svg" alt="icone livre">
        </div>
        <span onclick="javascript:if (confirm('You are leaving this webpage')) { window.location.href='https://formations.univ-rennes2.fr/fr/formations/master-37/master-mention-histoire-civilisations-patrimoine-JFKR2UKZ.html';};">International Relations Master's degree</span>
        <p>Rennes 2 university (2014-2019)</p>
      </div>
    </div>
  </div>
  <!--***************** Expériences ********************-->

  <!--***************** Fin du Main *****************-->
  <!-- Début formulaire -->
  <div class="row" id="formulaireContact">
    <form method="post" class="form-container col-md" action="index-en.php" itemscope itemtype="https://schema.org/ContactPage">
      <h1>Contact Form</h1>
      <br>
      <p style="color: red;">* Required fields</p>
      <br>
      <label for="givenName">
        Name
      </label><br>
      <input itemprop="sender" type="text" placeholder="Your name" name="givenName" id="givenName" autofocus required>
      <span style="color: red;">*</span>

      <br>
      <label for="email">
        Email
      </label><br>
      <input itemprop="email" type="email" placeholder="Your email" name="email" id="email" required>
      <span style="color: red;">*</span>
      <br>
      <label for="sujet">
        Subject
      </label><br>
      <input itemprop="about" type="text" placeholder="Subject of your message" name="sujet" id="sujet" required>
      <span style="color: red;">*</span>

      <br>
      <label for="message">
        Your message
      </label><br>
      <textarea itemprpop="messageAttachment" name="message" style="height:200px; width:100%" id="message" required></textarea>
      <span style="color: red;">*</span>

      <button type="submit" class="btn">Send</button>
      <button type="submit" class="btn cancel" onclick="closeForm()">Cancel</button>
    </form>
  </div>
  <?php
  // Email pour Ivan 
  $msg = wordwrap($_POST['message'], 70);
  if (isset($_POST['message'])) {
    if (@mail('ivan.bethus@gmail.com', 'Contact: ' . $_POST['sujet'] . ' de: ' . $_POST['givenName'], $msg, 'From: ' . $_POST['email'])) {
      echo '<script>alert("Your email was sent !")</script>';
    }
  }
  // Email pour l'utilisateur 
  $headers = "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/html; charset=UTF-8-1\r\n";
  $UserMessage .= '<html>';
  $UserMessage .= "<head><style>p, span {font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;}</style></head>";
  $UserMessage .= '<body>';
  $UserMessage .= '<div style="border:3px solid black; background-color:rgb(233, 233, 233); padding:2rem; width:50%; box-shadow: 5px 5px 30px grey; margin:auto; margin-top: 2rem; margin-bottom:2rem; border-radius: 80px 2px;">';
  $UserMessage .= "<span>Hi </span>" . strip_tags($_POST['givenName']) . "<span>,</span>";
  $UserMessage .= "<br><p>Thanks for contacting me, I'll answer you as soon as possible.</p>";
  $UserMessage .= '<br><p>Regards,</p>';
  $UserMessage .= '<br><p>Ivan Béthus</p>';
  $UserMessage .= '</body></html>';

  mail($_POST['email'], 'Contact', $UserMessage, $headers)

  ?>
  <!-- Fin formulaire -->

  <!--***************** Footer *****************-->
  <footer>
    <div id="contact" class="row">
      <div class="col" style="text-align: center">
        <img src="img/icones/linkedin.svg" onclick="javascript:if (confirm('You are leaving this webpage')) { window.location.href='http://www.linkedin.com/in/ivan-béthus-570067b2';};" alt="logo LinkedIn">
      </div>
      <div class="col" style="text-align: center;">
        <img src="img/icones/twitter.svg" onclick="javascript:if (confirm('You are leaving this webpage')) { window.location.href='https://twitter.com/IBethus';};" alt="logo Twitter">
      </div>
      <div class="col" style="text-align: center">
        <a href="#formulaireContact">
          <img src="img/icones/email.svg" onclick="openForm()" alt="Bouton ouverture formulaire">
        </a>
      </div>
    </div>
    <div id="mentions" class="row">
      <div class="col">
        <p>© Ivan Béthus 2019</p>
      </div>
      <div class="col">
        <a href="mentions.html">Legal Notice</a>
      </div>
      <div class="col">
        <a href="CREDITS.html">Credits</a>
      </div>
    </div>
  </footer>
  <!--***************** Fin footer *****************-->

  <!-- librairie jQuery -->
  <script src="js/vendor/jquery-3.4.1.min.js"></script>

  <!-- Bootstrap scripts-->
  <script src="js/vendor/bootstrap.bundle.min.js"></script>

  <!-- scripts additionnels -->
  <script src="js/script.min.js"></script>
</body>

</html>